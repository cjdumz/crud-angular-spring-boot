import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  
    public firstName: string;
    public password: string;

  constructor(private http: HttpClient) {

  }   

  login(firstName: string, password: string) {
    return this.http.get(environment.hostUrl + `/login`,
      { headers: { authorization: this.createBasicAuthToken(firstName, password) } }).pipe(map((res) => {
        this.firstName = firstName;
        this.password = password;
        this.registerSuccessfulLogin(firstName, password);
      }));
  }


  createBasicAuthToken(firstName: string, password: string) {
    return 'Basic ' + window.btoa(firstName + ":" + password); 
  }

  registerSuccessfulLogin(firstName, password) {
    // save the username to session
  }
}