import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'Spring Boot - Angular';

  constructor(private _router:Router){}
  
  ngOnInit() {

    
  }

  loggedIn(){
    return localStorage.getItem('token');
  }
  
  onLogout(){
    localStorage.removeItem('token');
    this._router.navigate(['/login'])
  }

}
