import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Employee } from '../employee';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})
export class CreateEmployeeComponent implements OnInit {

  msg='';
  employee: Employee = new Employee();
  constructor(private employeeService: EmployeeService, private router: Router) { }
  exform: FormGroup;
  ngOnInit(): void {
    this.exform = new FormGroup({
      'firstName' : new FormControl(null, Validators.required),
      'lastName': new FormControl(null, Validators.required),
      'email' : new FormControl(null, [Validators.required, Validators.email])
    });
  }

  saveEmployee(){
    this.employeeService.createEmployee(this.employee).subscribe( data =>{
      console.log(data);
      this.goToEmployeeList();
    },
    error => console.log(error));
    console.log("User already Exists.");
        this.msg="User already Exists.";
  }

  goToEmployeeList(){
    this.router.navigate(['/login']);
  }

  onSubmit(){
    this.saveEmployee();
    // console.log(this.employee);
  }
}
