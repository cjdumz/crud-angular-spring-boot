import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Employee } from '../employee';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  
  msg='';
  employee = new Employee();

  constructor(private _employeeService: EmployeeService, private _router:Router) {}

  ngOnInit(): void {
  }

  loginUser(loginForm: NgForm){
    console.log(loginForm.value);
    const token = this._employeeService.authUser(loginForm.value);
    this._employeeService.loginEmployeeFromRemote(this.employee).subscribe(
      data => {
        localStorage.setItem('token', JSON.stringify(token))
        console.log("Login Successful");
        this._router.navigate(['/employees'])
      },
      error =>{
        console.log("Wrong Credentials");
        this.msg="Wrong Credentials";
      }  
      )
  }
  loggedIn(){
    return localStorage.getItem('token');
  }
  
  onLogout(){
    localStorage.removeItem('token');
    this._router.navigate(['/login'])
  }
 
}
