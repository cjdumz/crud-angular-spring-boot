/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package crud.application.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 *   
 * @author cjdum
 */
@Configuration
@EnableWebMvc
public class CorsConfig implements WebMvcConfigurer {

//    @Override
//    public void addCorsMappings(CorsRegistry corsRegistry) {
//        corsRegistry.addMapping("/**")
//                .allowedOrigins("http://localhost:4200")
//                .allowedMethods("GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS").allowedOrigins("*")
//                .maxAge(3600L)
//                .allowedHeaders("*")
//                .exposedHeaders("Authorization")
//                .allowCredentials(true);
//    }
////    
////    public class SecurityConfig extends WebSecurityConfigurerAdapter {
////    @Override
////    public void configure(HttpSecurity httpSecurity) throws Exception{
////        httpSecurity.csrf().disable()
////        .authorizeRequests()
////        .antMatchers("/api1/**").permitAll()
////        .antMatchers("/api2/**").permitAll()
////        .antMatchers("/api3/**").permitAll();   
////        
////}
//    
//    @Bean
//    public WebMvcConfigurer corsConfigurer() {
//        return new WebMvcConfigurerAdapter() {
//            @Override
//            public void addCorsMappings(CorsRegistry registry) {
//                registry.addMapping("/**").allowedOrigins("*");
//            }
//        };
//    }
//    
////    @Bean
////    public WebMvcConfigurer corsConfigurer(){
////        return new WebMvcConfigurer(){
////            
////            @Override
////            public void addCorsMappings(CorsRegistry registry){
////                registry.addMapping("/**")
////                        .allowedMethods("GET", "POST", "PUT", "DELETE")
////                        .allowedHeaders("*")
////                        .allowedOrigins("http://localhost:4200/");
////            }
////        };
////    }
//}
}
