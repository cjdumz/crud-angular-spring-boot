/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package crud.application.controller;

import crud.application.exception.ResourceNotFoundException;
import crud.application.model.Employee;
import crud.application.repository.EmployeeRepository;
import crud.application.service.EmployeeService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 *
 * @author cjdum
 */

@RestController
//@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/")
public class EmployeeController {
    
//     @Bean
//    public WebMvcConfigurer corsConfigurer() {
//        return new WebMvcConfigurerAdapter() {
//            @Override
//            public void addCorsMappings(CorsRegistry registry) {
//                registry.addMapping("/**").allowedOrigins("*");
//            }
//        };
//    }
    
    @Autowired
    private EmployeeRepository employeeRepository;
    
    @Autowired
    private EmployeeService employeeService;

    
    // get all employees
    @GetMapping("/employees")
    public List<Employee> getAllEmployee(){
        return employeeRepository.findAll();
    }
    
    // create employee
    @PostMapping("/employees")
    public Employee creatEmployee(@RequestBody Employee employee) throws Exception{
        if(employeeRepository.findByEmailAndUsername(employee.getEmail(), employee.getUsername()) !=null) {
            throw new Exception("User already exists.");
        }else{
            return employeeService.saveUser(employee);
        }
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/loginAuth" , method = RequestMethod.GET)
    public Employee loginUser(@RequestBody Employee employee) throws Exception {
        String tempEmail = employee.getUsername();
        String tempPass = employee.getPassword();
        Employee userObj = null;
        if(tempEmail != null && tempPass !=null){
            userObj = employeeService.fetchUserByEmailAndPassword(tempEmail, tempPass);
        }
        if(userObj == null){
            throw new Exception("User does not exist.");
        }
        return userObj;
    }
    
    @RequestMapping(value="/logout", method = RequestMethod.POST)
    public String logout(HttpServletRequest request, HttpServletResponse response){
        return "redirect:/login";
    }
    
    //get employee by name
//    @GetMapping("employees/{firstName}")
//    public ResponseEntity<Employee> getEmployeeByName(@PathVariable String firstName){
//        Employee employee = employeeRepository.findOne(firstName)
//                .orElseThrow(() -> new ResourceNotFoundException("User does not exists with ID: "+ firstName));
//        return ResponseEntity.ok(employee);
//    
    
    // get employee by id
    @GetMapping("employees/{id}")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable Long id){
        Employee employee = employeeRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User does not exists with ID: "+ id));
        return ResponseEntity.ok(employee);
    }
    
    // update employee
    @RequestMapping(value="/employees/{id}", method = RequestMethod.POST)
    public ResponseEntity<Employee> updateEmployee(@PathVariable Long id, @RequestBody Employee employeeDetails){
         Employee employee = employeeRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User does not exists with ID: "+ id));
         
         employee.setFirstName(employeeDetails.getFirstName());
         employee.setLastName(employeeDetails.getLastName());
         employee.setEmail(employeeDetails.getEmail());
         employee.setUsername(employeeDetails.getUsername());
         
         Employee updatedEmployee = employeeRepository.save(employee);
         return ResponseEntity.ok(updatedEmployee);
    }
    
    //delete employee
    @DeleteMapping("/employees/{id}")
    public ResponseEntity<Map<String, Boolean>> deleteEmployee(@PathVariable Long id){
         Employee employee = employeeRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User does not exists with ID: "+ id));
         
         employeeRepository.delete(employee);
         Map<String, Boolean> response = new HashMap<>();
         response.put("deleted", Boolean.TRUE);
         return ResponseEntity.ok(response);
    }
    
    //search employee
    
}
