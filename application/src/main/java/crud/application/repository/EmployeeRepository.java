/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package crud.application.repository;

import crud.application.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author cjdum
 */


public interface EmployeeRepository extends JpaRepository<Employee, Long>{
    
    public Employee findByEmail(String email);
    public Employee findByEmailAndPassword(String email, String password);
    public Employee findByUsername(String username);
    public Employee findByEmailAndUsername(String email, String username);
    
}
