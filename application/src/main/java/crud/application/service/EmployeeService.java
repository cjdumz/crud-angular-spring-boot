/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package crud.application.service;

import crud.application.model.Employee;
import crud.application.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 *
 * @author cjdum
 */
@Service
public class EmployeeService {
    
    @Autowired
    private EmployeeRepository repo;
    
    PasswordEncoder passwordEncoder;
    EmployeeRepository employeeRepository;
    
    public EmployeeService(EmployeeRepository employeeRepository){
        this.employeeRepository = employeeRepository;
        this.passwordEncoder = new BCryptPasswordEncoder();
    }
    
    public Employee saveUser(Employee employee){
        String encodedPassword = this.passwordEncoder.encode(employee.getPassword());
        employee.setPassword(encodedPassword);
        return repo.save(employee);
    }
    
    public Employee fetchUserByEmail(String username){
        return repo.findByEmail(username);
    }
    
    public Employee fetchUserByEmailAndPassword(String email, String password){
        return repo.findByEmailAndPassword(email, password);
    
    }
    
    public Employee fetchUserByEmailAndUsername(String email, String username){
        return repo.findByEmailAndUsername(email, username);
    }
}
 